import Vue from 'vue';
import App from './App.vue';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import i18n from './i18n';
import SRAgen from "./components/SRAgen";

Vue.use(Buefy);
Vue.component("sragen", SRAgen);
Vue.config.productionTip = false;

new Vue({
  i18n,
  //pdfform,
  render: h => h(App)
}).$mount('#app')
